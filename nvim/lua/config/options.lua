-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.opt.filetype.plugin = "on"
vim.opt.filetype.indent = "on"
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.isfname:append("@-@")
vim.opt.colorcolumn = "120"
vim.cmd([[hi NormalNC guibg=NONE]])
